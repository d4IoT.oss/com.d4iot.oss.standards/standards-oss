 Various artifacts used to support development and build life cycles

 
# IDE
The following IDE standards are followed.
## Eclipse
Download and install eclipse. Modify eclipse.ini to point to JDK and memory settings (see https://wiki.eclipse.org/Eclipse.ini)

Start Eclipse and switch to your permanent workspace

* Make the following Changes Under Preferences
  * general->Workspace
     * text file encoding - UTF-8
     * text file delimiter - unix
  * general->Editors->Text Editors
     * insert spaces for tabs - enable
  * general_>startup/shutdown
     * refresh workspace on startup - enable
  * java->installed JREs
     * pick correct JDK/JRE as default
  * java->compiler
     * jdk compliance 1.8


* Under Help->install 
  * from marketplace  (Version specified are minimum)
      * JAutoDoc(1.14.0)
      * SonarLint (2.5.1)
      * Markdown Editor (winterwell 1.2)
* Under Prefernces->maven->discovery-open Catalog, install the following m2e plugins
      * m2e-egit

* under Prefernce
   * java->code Style
         * cleanup -> use [cleanup template](src/main/resources/ide/eclipse/cleanup.xml)
         * format -> use [format template](src/main/resources/ide/eclipse/formatter.xml)
         * code templates -> use [code template](src/main/resources/ide/eclipse/codetemplates.xml)
   * jautodoc -> use [jautodoc template](src/main/resources/ide/eclipse/jautodoc_preferences.xml)
   * java->editor->saveActions [Setup](src/main/resources/ide/eclipse/JavaEditorSaveActions.png)
   * java->Appearance->Member SortOrder [Setup](src/main/resources/ide/eclipse/JavaMemberSortOrder.png)
   
